# Instructions
Install [Node.js](https://nodejs.org/)

Install the dependencies and devDependencies.
```sh
$ npm install
```

## Automaticaly Compile Less > css
```sh
$ gulp watch
```

This creates two copies of the css on save of any LESS file
```sh
style.css
```
and
```sh
css/style.min.css
```

## Manualy Compile Less > css
```sh
$ npm run build-css
```

This creates two copies of the css
```sh
style.css
```
and
```sh
css/style.min.css
```

The minified version is used by the theme