<?php
/*
 *  Author: Alex Whitlock
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
External Modules/Files
\*------------------------------------*/

// ACF Setup
require_once( __DIR__ . '/inc/acf/acf-setup.php');

/*------------------------------------*\
Theme Support
\*------------------------------------*/

if (!isset($content_width)) {$content_width = 1100;}
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
    add_image_size('post-thumb-size', 500, 220, true);
    add_theme_support('menus');
    add_theme_support('automatic-feed-links');
    load_theme_textdomain('dreem_lang', get_template_directory() . '/languages');
}

/*------------------------------------*\
Functions
\*------------------------------------*/

// Register Navigation
function register_menus()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'main-menu' => 'Main Menu',
    ));
}

// Load styles
function base_css()
{
    //wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:400,700');
    wp_enqueue_style('base_style', get_template_directory_uri() . '/css/style.min.css', array(), '1.0', 'all'); // The main minified CSS is stored here. Non-min is in root theme
    // wp_enqueue_style('fontCSS', get_template_directory_uri() . '/css/fontello.css', array(), '1.0', 'all');
    // wp_enqueue_style( 'dashicons-style', get_stylesheet_uri(), array('dashicons'), '1.0' );
}

// Load scripts
function header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
        wp_enqueue_script('global_header_script', get_template_directory_uri() . '/js/header-scripts.js', array(), '3.5.0');
    }
}

// Load scripts
function footer_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
        wp_enqueue_script('global_footer_script', get_template_directory_uri() . '/js/footer-scripts.js', array('jquery'), '1.0', true);
    }
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }
    return $classes;
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages,
    ));
}

// Create the Custom Excerpts callback
function html5wp_index($length)
{return 20;}
function html5wp_custom_post($length)
{return 40;}

function html5wp_excerpt($length_callback = '', $more_callback = '')
{global $post;
    if (function_exists($length_callback)) {add_filter('excerpt_length', $length_callback);}
    if (function_exists($more_callback)) {add_filter('excerpt_more', $more_callback);}
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Remove Admin bar
function remove_admin_bar()
{return false;}

// Class on Mobile devlice */
function my_body_classes($classes)
{$classes[] = 'mobile_device';return $classes;}

/*------------------------------------*\
Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('wp_enqueue_scripts', 'base_css'); // Add Theme Stylesheet
add_action('init', 'header_scripts'); // Add Custom Scripts to wp_head
add_action('init', 'register_menus'); // Add HTML5 Blank Menu
add_action('wp_footer', 'footer_scripts', 15);
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
if (wp_is_mobile()) {add_filter('body_class', 'my_body_classes');}; //If Mobile add class
