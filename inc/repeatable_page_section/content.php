<?php

// check if the flexible content field has rows of data
if (have_rows('page_content')):

    // loop through the rows of data
    while (have_rows('page_content')): the_row();

        if (get_row_layout() == 'full_width_content'): ?>
            
            <div class="large-section <?php the_sub_field('background_colour'); ?>"><div class="container">
                <?php the_sub_field('full_width_content'); ?>
            </div></div>

        <?php
        elseif (get_row_layout() == 'split_image__content'): ?>
            
            <div class="large-section <?php the_sub_field('background_colour'); ?>"><div class="container">
                <?php // check if the nested repeater field has rows of data
                if (have_rows('split_image_section')):
                    // loop through the rows of data
                    while (have_rows('split_image_section')): the_row(); 

                        if( get_sub_field('image_position') === 'left' ): ?>
                        <div class="split_image_section">
                            <div class="split_image_section--image"><i style="background-image:url(<?php the_sub_field('split_image__section__image')?>);"></i></div>
                                <div class="split_image_section--content">
                                <?php the_sub_field('split_image__section__content'); ?>
                            </div>
                        </div>
                        <?php else: ?>
                        <div class="small-section">
                            <div class="split_image_section split_image_section--image-on-top">
                                <div class="split_image_section--content">
                                <?php the_sub_field('split_image__section__content')?>
                                </div>
                                <div class="split_image_section--image"><i style="background-image:url(<?php the_sub_field('split_image__section__image')?>);"></i></div>
                            </div>
                        </div>

                        <?php endif; ?>

                    <?php endwhile;
                endif; ?>
            </div></div>

       <?php endif;

    endwhile;

endif; ?>
