<?php
/*
 *  Page Heading
 */
?>
<div class="page-heading"><div class="page-heading--inner"><div class="container">
  <h1><?php get_field('page_headline') ? the_field('page_headline') : the_title();?></h1>
</div></div></div>