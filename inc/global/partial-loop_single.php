<li class="small--post--article"><article id="post-<?php the_ID(); ?>"><a href="<?php the_permalink(); ?>">
    <?php if ( has_post_thumbnail()) : the_post_thumbnail('square-size'); endif; ?>
    <div class="small--post--article--inner">
        <h3><?php the_title(); ?></h3>
        <div class="fade-content">
            <?php html5wp_excerpt('html5wp_custom'); ?>
        </div>
        <span class="button">Read Post</span>
    </div>
</a></article></li>