<?php get_header();?>
	<main class='main'>
	<?php if (have_posts()): while (have_posts()): the_post();?>
		<article id="post-<?php the_ID();?>" <?php post_class();?>>

			<?php get_template_part('inc/global/heading'); // Repeatable Page content ?>

			<div class="small-section"><div class="container"><?php the_content();?></div></div>

			<?php get_template_part('inc/repeatable_page_section/content'); // Repeatable Page content ?>

			<?php if( current_user_can('edit_others_pages') ) :?>
			<div class="white-section small-section"><div class="container"><?php edit_post_link();?></div></div>
  			<?php endif;?>

		</article>
	<?php endwhile; endif;?>
	</main>
<?php get_footer();?>
