<?php get_header(); ?>

	<main role="main">
	<!-- section -->
	<section>
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="post-intro-heading" style="background-image:url(<?php if ( has_post_thumbnail()) : the_post_thumbnail_url(); endif; ?>)">
				<div class="post-intro-inner">
					<div class="container">
						<h1><?php the_title(); ?></h1>
						<span class="author"><?php _e( 'By', 'dreem_lang' ); ?> <?php the_author(); ?></span> <?php _e( 'on', 'dreem_lang' ); ?> <span class="date"><?php the_time('F j, Y'); ?></span> <span>| <?php the_category(', '); ?></span>
					</div>
				</div>
			</div>

			<div class="article-body small-section">
				<div class="container">
					<?php the_content(); // Dynamic Content ?>
					<?php edit_post_link(); // Always handy to have Edit Post Links available ?>
				</div>
			</div>
		</article>
		<!-- /article -->
	<?php endwhile; ?>
	<?php else: ?>
		<!-- article -->
		<article>
			<h1><?php _e( 'Sorry, nothing to display.', 'dreem_lang' ); ?></h1>
		</article>
		<!-- /article -->
	<?php endif; ?>
	</section>

	<div class="container"><h2><?php _e( 'Recent Posts', 'dreem_lang' ); ?></h2></div>
	<div class="container no--padding">
		<ul class="recent--posts clearfix">
			<?php
			$args = array( 'post_status' => 'publish', 'numberposts' => '2', 'post__not_in' => array( $post->ID ));
			$recent_posts = wp_get_recent_posts( $args );
			foreach( $recent_posts as $recent ) {
				echo '<li class="small--post--article"><a href="'.get_permalink($recent["ID"]) .'">';
				if ( has_post_thumbnail()) : echo get_the_post_thumbnail($recent["ID"], 'post-thumb-size'); endif;
				echo '<h3>' . $recent["post_title"] . '</h3>';
				echo '</a></li>';
			}
			wp_reset_query();
			?>
		</ul>
	</div>

	<!-- /section -->
	</main>
<?php get_footer(); ?>
