<?php get_header(); ?>

<main role="main" class="article-body small-section">
<!-- section -->
<section>

	<div class="container"><h1><?php _e( 'Categories for ', 'dreem_lang' ); single_cat_title(); ?></h1></div>

	<div class="container no--padding">
		<div class="recent--posts clearfix">
			<?php get_template_part('loop'); ?>
		</div>
	</div>
	
	<div class="container"><?php get_template_part('pagination'); ?></div>

</section>
<!-- /section -->
</main>

<?php get_footer(); ?>
