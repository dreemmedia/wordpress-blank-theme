const gulp = require('gulp');
const less = require('gulp-less');
const rename = require("gulp-rename");
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const sftp = require('gulp-sftp');

const LessPluginCleanCSS = require('less-plugin-clean-css'),
  cleanCSSPlugin = new LessPluginCleanCSS({
    advanced: true
  });
const LessAutoprefix = require('less-plugin-autoprefix'),
  autoprefix = new LessAutoprefix({
    browsers: ['ie >= 9', 'last 2 versions']
  });
const groupMediaQueries = require('less-plugin-group-css-media-queries');


// Parse LESS and compress to style.min.css
gulp.task('less-compress', function () {
  return gulp.src(['./less/wordpress-header.less', './less/*/*.less', './inc/**/*.less'])
    .pipe(less({
      plugins: [autoprefix, groupMediaQueries, cleanCSSPlugin]
    }))
    .pipe(concat('style.min.css'))
    .pipe(gulp.dest('./css/'));
});

// Parse LESS output to style.css for wordpress theme info
gulp.task('less', function () {
  return gulp.src(['./less/wordpress-header.less', './less/*/*.less', './inc/**/*.less'])
    .pipe(less({
      plugins: [autoprefix, groupMediaQueries]
    }))
    .pipe(concat("style.css"))
    .pipe(gulp.dest('./'));
});


// // Parse Login LESS and compress to login.css
// gulp.task('less-login-compress', function () {
//  return gulp.src('./less/login.less')
//   .pipe(less({
//     plugins: [cleanCSSPlugin]
//   }))
//   .pipe(rename("login.min.css"))
//   .pipe(gulp.dest('./css/'));
// });

// Parse all HEADER scripts and put into header_scripts.min.js
gulp.task('header-scripts', () =>
  gulp.src('./js/**/*header.js')
  .pipe(babel({
    "presets": ["minify"]
  }))
  .pipe(concat('header-scripts.js'))
  .pipe(gulp.dest('./js/'))
);

// Parse all HEADER scripts and put into header_scripts.min.js
gulp.task('footer-scripts', () =>
  gulp.src('./js/**/*footer.js')
  .pipe(babel({
    "presets": ["minify"]
  }))
  .pipe(concat('footer-scripts.js'))
  .pipe(gulp.dest('./js/'))
);

// // SFTP all files to remote
// var host = 'xxx.xxx', 
//   auth = 'keyMain', 
//   remotePath = '/srv/users/serverpilot/apps/xxx';

// gulp.task('compile', function (){
//   return gulp.src(['!node_modules/**','./**'])
//     .pipe(gulp.dest('./'))
//     .pipe(sftp({
//       host: host,
//       auth: auth,
//       remotePath: remotePath
//     }));
// });


// Watch all the above tasks
gulp.task('watch', function () {
  gulp.watch(['./**/*.less', './**/*.js'], ['less', 'less-compress', 'header-scripts', 'footer-scripts']);
  // gulp.watch(['!node_modules/**', './**], ['sftp']);
  // gulp.watch(['!./node_modules/**', '!./css/**', '!./i/**', './**'], ['sftp-quick-dev']);
});


gulp.task('default', ['less', 'less-compress', 'header-scripts', 'footer-scripts', 'watch']);