<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php get_field('page_title') ? the_field('page_title') : wp_title(''); ?><?php if(!is_front_page()): if(wp_title('', false)) { echo ' -'; } ?> <?php bloginfo('name'); endif; ?></title>
	<meta name="description" content="<?php get_field('page_description') ? the_field('page_description') : bloginfo('description'); ?>">
	<link href="//www.google-analytics.com" rel="dns-prefetch">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	
	<?php /*
	Mobile Menu - Clone of Main Menu
	mp-menu 
	*/ ?>
	<nav id="mp-menu" class="mp-menu">
		<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
	</nav>
	
	<div id="top" class="wrapper">
		<!-- header -->
		<header class="header clear" role="banner"><div class="container clear clearfix">
			<div class="logo">
				<a href="<?php echo home_url(); ?>">
					<?php bloginfo('name'); ?>
				</a>
			</div>
			<nav class="nav" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
			</nav>

			<div id="trigger" class="menu-trigger">Open/Close Menu</div>
		</div></header>
		<!-- /header -->