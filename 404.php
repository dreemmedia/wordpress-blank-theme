<?php get_header();?>
	<main class='main'>
			<article id="post-404" class='post-404 large-section'><div class="container">
				<h1><?php _e( 'Page not found', 'dreem_lang' ); ?></h1>
				<h2><a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'dreem_lang' ); ?></a></h2>
			</div></article>
	</main>
<?php get_footer();?>