(function ($, root, undefined) {
  $(function () {
    'use strict';

    /* Adds Smooth Scroll to class name .jump-button to target href */
    $('a.jump-button, li.jump-button a').on('mousedown touchstart', function () {
      $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
      }, 800);
      return false;
    });

    
    /* Main Menu */
    $('.nav .menu-item-has-children > a:first-child').bind('click', false);
    $('.nav .menu-item-has-children > a:first-child').on('mousedown touchstart', function (e) {
      $('.nav .menu-item-has-children > a:first-child').parent().removeClass("current_hover");
      e.stopPropagation();
      e.preventDefault();
      $(this).parent().addClass("current_hover");
    });

    $('#top').on('touchstart mousedown', function (e) {
      $('.nav .menu-item-has-children > a:first-child').parent().removeClass("current_hover");
      if ($("#mp-menu").hasClass("menuActive")) {
        e.preventDefault();
        $("#mp-menu").removeClass("menuActive");
        $('#top').removeClass("wrapperFade");
      }
    });

    $('.nav').on('touchstart mousedown', function (e) {
      e.stopPropagation(); // Keeps clicks meant for the menu on the menu
    });


    /* Mobile Menu - Adds touch events, including slide open and cancel  */
    $('#trigger').on('touchstart mousedown', function (e) {
      e.stopPropagation();
      e.preventDefault();
      $("#mp-menu").toggleClass("menuActive");
      $('#top').addClass("wrapperFade");
    });

    /* Adds Go back button on sub-menus */
    $("#mp-menu .sub-menu").prepend("<li class='gobacktoprevious' ><a href='#'>< Back</a></li>");
    $('.gobacktoprevious a').on("touchstart mousedown", function (e) {
      e.preventDefault();
      $(this).parent().parent().removeClass("subMenuActive");
    });

    $('#mp-menu li.menu-item-has-children > a').on("touchstart mousedown", function (e) {
      e.preventDefault();
      $(this).next(".sub-menu").addClass("subMenuActive");
    });
    /* End Menu */


  });
})(jQuery, this);