	<!-- footer -->
	<footer class="footer" role="contentinfo">
		<div class="copyright">
			<div class="container clearfix">
				<p>&copy; <?php echo date('Y'); ?> Copyright <?php bloginfo('name'); ?></p>
				<p class="dreem_info"><a rel="nofollow" href="http://dreem-media.co.uk/?utm_source=Blank&amp;utm_medium=Site%20Footer&amp;utm_campaign=Site%20Footer"><span class="hide">Website Design &amp; Development by:</span><i>Dreem Media</i></a></p>
			</div>
		</div>
	</footer>
	<!-- /footer -->

</div>
<!-- /wrapper -->
<?php wp_footer(); ?>
</body>
</html>
