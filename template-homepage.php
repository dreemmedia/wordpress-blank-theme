<?php /* Template Name: Homepage */ get_header(); ?>
	<main class='main' id='main'>

	<?php if( current_user_can('edit_others_pages') ) :?>
	<div class="white-section small-section"><div class="container"><?php edit_post_link();?></div></div>
	<?php endif;?>
	
	</main>
<?php get_footer(); ?>

